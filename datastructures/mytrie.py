# Trie (prefix tree) is tree datastructure is used to insert/seach/delete of keys (strings) efficiently in large dataset. 
# we store the items with prefixes in common


class Node:
    def __init__(self) -> None:
        self.children = {str:Node}  # letter and corresponding node
        self.endofword = False

class Trie:
    def __init__(self) -> None:
        self.root = Node()
    
    def insert(self, word: str) -> None:
        ptr = self.root

        for letter in word:
            if letter not in ptr.children:
                ptr.children[letter] = Node()
            ptr = ptr.children[letter]
        ptr.endofword = True
        
    
    def search(self,word:str) -> bool:
        ptr = self.root
        for letter in word:
            if letter not in ptr.children:
                return False
            ptr = ptr.children[letter]
        if ptr.endofword:
            return True
        else:
            return False

    '''
    returns true if any word in trie starts with the given word
    '''
    def startswith(self,prefix:str) -> None:
        ptr = self.root
        for letter in prefix:
            if letter not in ptr.children:
                return False
            ptr = ptr.children[letter]
        return True

if __name__ == "__main__":
    seed = 1
    next = 1
    while seed <= 9999:
        next = seed + next
        seed = next
        print(seed)
