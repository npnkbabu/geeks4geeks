# create custom dict


class MyDict(dict):

    def __init__(self,mapping=None,/,**kwargs):
        if mapping is not None:
            mapping = {
                x.upper():y for x,y in mapping.items()
            }
        else:
            mapping = {}
        if kwargs:
            mapping.update({
                x.upper():y for x,y in mapping.items()
            })
        super().__init__(mapping)
    def __setitem__(self, key: str, value: int) -> None:
        key = key.upper()
        return super().__setitem__(key, value)
    
    def __getitem__(self, key: str) -> int:
        key = key.upper()
        return super().__getitem__(key)


from collections import UserDict

class MyUserDict(UserDict):
    def __setitem__(self, key: str, item: int) -> None:
        key = key.upper()
        return 


class VaulueDict(dict):

    # it returns key depending on val
    def key_of(self,val):
        for k,v in self.items():
            if v == val:
                return k
        return ValueError(val)
    
    # returns keys for  vals
    def keys_of(self,val):
        for k,v in self.items():
            if v == val:
                yield k
        return ValueError(val)
    
if __name__ == "__main__":
    # finding key from value. values are stored in a list with index same as the keys list index (hashed).
    #val = "Two"
    #key = (list(mydict.keys()))[list(mydict.values()).index(val)]
    mydict = VaulueDict({ "one":1})
    mydict["two"]=2
    mydict["TWO"]=2
    mydict.update({"three":3})
    print(list(mydict.keys_of(2)))
