#https://www.geeksforgeeks.org/graph-data-structure-and-algorithms/
# graphs can be represented by adjecency lists or adjecency matrix
# we are taking adjecency lists, means list of linked lists.

# you need to keep a visited array to handle cyclic graphs

# add egde for non-directional graph
def add_edge(graph,n1, n2):
    graph[n1].append(n2)
    graph[n2].append(n1)


# we take queue, s: source
def bfs(graph,s):
    stack = []
    stack.append(s)
    visited = []

    while stack:
        node = stack.pop(0) # since it queue
        if node not in visited:
            visited.append(node)
            print(node)
            for neighbour in graph[node]:
                stack.append(neighbour)
        else:
            print("cyclic")

# we take stack to backtrack, s: source
def dfs(graph,s):
    stack = []
    stack.append(s)
    visited = []

    while stack:
        node = stack.pop(-1)
        if node not in visited:
            visited.append(node)
            print(node)
            for neighbour in graph[node]:
                stack.append(neighbour)
        else:
            print("cyclic")

# dijkstra algo: finding shortest path from a source node to all nodes in weighted directed or non-directed graph
# n : no.of vertices, edge is tuple (u,v,w) u: source, v : dest, w: weight, given src
def dijkstra(graph,src)-> dict[int,int]:
    pass



class Graph:
    def __init__(self) -> None:
        self.adjecency_list = {} # vertex: [edges]

class Vertex:
    def __init__(self) -> None:
        self.value = None

class Edge:
    def __init__(self) -> None:
        self.distance = None
        self.vertex = None

 

if __name__ == "__main__":
    print("hello")

    # directed non cyclic graph
    adjecency_list = {
        "a" : ["b","d"],
        "b" : [],
        "c" : [],
        "d" : ["e", "g"],
        "e" : ["c"],
        "f" : ["c"],
        "g" : ["f"]
    }
    adjecency_list_weighted = {
            "a" : [("b",2),("d",3)],
            "b" : [("d",4)],
            "c" : [("a",1)],
            "d" : [("e",1), ("g",3)],
            "e" : [("c",2)],
            "f" : [("c",1)],
            "g" : [("f",2)]
        }
    dijkstra(adjecency_list_weighted,"a")
