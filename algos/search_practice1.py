# O(n)
def linearSearch(arr,a):
    for i in arr:
        if i == a:
            return True
    return False

#O(logn) as we divide into half everytime
def BinarySearch_rec(arr,a):
    n = len(arr)
    if n == 0:
        return False
    if n == 1:
        if arr[0] != a:
            return False
        else:
            return True
    half = n//2
    if a  == arr[half]:
        return True
    if a < arr[half]:
        arr = arr[:half]
    else:
        arr = arr[half:]
    return BinarySearch_rec(arr,a)

def BinarySearch(arr,a):
    n = len(arr)
    if n == 0:
        return False
    if n == 1:
        if arr[0] == a:
            return True
        else:
            return False
    while n > 0:
        if n == 1:
            if arr[0] == a:
                return True
            else:
                return False
        half = n//2
        if a < arr[half]:
            arr = arr[:half]
        else:
            arr = arr[half:]
        n = len(arr)
        

if __name__ == "__main__":
    arr = [2,3,5,8,12,1,4]
    a = 42
    print(BinarySearch(sorted(arr),a))