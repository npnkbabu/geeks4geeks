
class TreeNode:
    def __init__(self,value) -> None:
        self.value = value
        self.left = None
        self.right = None

    # left < root < right
    def insert(self,value):
        if value < self.value:
            if self.left == None:
                self.left = TreeNode(value)
            else:
                self.left.insert(value)
        else:
            if self.right == None:
                self.right = TreeNode(value)
            else:
                self.right.insert(value)

    def search(self,value):
        pass

    # left --> root --> right --> this gives in ascending order for binarysearch tree.
    def inorder(self):
        if self.left:
            self.left.inorder()
        print(self.value)
        if self.right:
            self.right.inorder()
    
    # root --> left --> right
    def preorder(self):
        print(self.value)
        if self.left:
            self.left.preorder()
        if self.right:
            self.right.preorder()
    
    # left -> right --> root
    def postorder(self):
        if self.left:
            self.left.postorder()
        if self.right:
            self.right.postorder()
        print(self.value)

# left --> root --> right
def DFS_inorder_stack(tree):
    stack = []
    ptr = tree
    res = []
    while ptr or stack:
        while ptr:
            stack.append(ptr)
            ptr = ptr.left
        ptr = stack.pop() # backtracking
        res.append(ptr.value)
        ptr = ptr.right
    return res

# root --> left --> right
def DFS_preorder_stack(tree):
    stack = []
    res = []
    ptr = tree
    while ptr or stack:
        if ptr:
            res.append(ptr.value)
            stack.append(ptr.right)
            ptr = ptr.left
        else:
            ptr = stack.pop()
    return res



# left --> right --> root
def DFS_postorder_stack(tree):
    stack = []
    res = []
    ptr = tree

    while ptr or stack:
        if ptr.right:
            tempptr = ptr
            if tempptr.right:
                ptr = ptr.right
            stack.append(ptr)
            stack.append(tempptr)
            if tempptr.left:
                stack.append(tempptr.left)
            else:
                res.append(tempptr.left.value)
                ptr = stack.pop()
    return res
        



    
        
        

if __name__ == "__main__":
    tree = TreeNode(3)
    tree.insert(4)
    tree.insert(5)
    tree.insert(8)
    tree.insert(9)
    tree.insert(1)
    tree.insert(10)
    tree.insert(2)
    tree.insert(12)
    tree.insert(6)
    tree.postorder()
    print(DFS_postorder_stack(tree))

