def linearSearch(arr,a):
    for i in arr:
        if i == a:
            return True
    return False

def BinarySearch(arr,a):
    if not arr:
        return False
    elif len(arr) == 1:
        if arr[0] == a:
            return True
        else:
            return False
    while(len(arr) > 1):
        n = len(arr)
        half = n//2
        if arr[half] == a:
            return True
        elif arr[half] > a:
            arr = arr[:half]
        else:
            arr = arr[half:]
    return False

        



if __name__ == "__main__":
    arr = [2,3,5,8,12,1,4]
    a = 811
    print(BinarySearch(arr,a))