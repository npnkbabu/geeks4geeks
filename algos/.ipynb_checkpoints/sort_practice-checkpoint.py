# O n*n
def BubbleSort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(n):
            if arr[i] < arr[j]:
                arr[i], arr[j] = arr[j], arr[i]
    return arr

def SelSort_test(arr):
    n = len(arr)
    min_index = 0
    for i in range(n):
        min_index = i
        for j in range(i+1,n):
            if arr[j] < arr[min_index]:
                min_index = j
        arr[i],arr[min_index] = arr[min_index],arr[i]
    return arr

# O n*n
def SelSort(arr):
    n = len(arr)
    min_index = 0
    for i in range(n):
        min_index = i
        for j in range(i+1,n):
            if arr[j] <= arr[min_index]:
                min_index = j
        arr[i], arr[min_index] = arr[min_index],arr[i]
    return arr

# O n*n
def InsSort(arr):
    n = len(arr)
    for i in range(n):
        key = arr[i]
        j = i+1
        while j<n:
            if key > arr[j]:
                arr[i], arr[j] = arr[j],arr[i]
            j += 1
    return arr

#O nlogn
def mergeSort(arr):
    n = len(arr)
    if n > 1:
        print(f"processing : {arr} for length {n}")
        
        # divide into 2 halfs untill can not divide
        half = n//2
        arr1 = mergeSort(arr[:half])
        arr2 = mergeSort(arr[half:])
        
        # merge with sorting
        i=0
        j=0
        k=0

        while (i<len(arr1) and j<len(arr2)):
            if arr1[i]<arr2[j]:
                arr[k] = arr1[i]
                i += 1
                k += 1
            else:
                arr[k] = arr2[j]
                j += 1
                k += 1
        while i< len(arr1):
            arr[k] = arr1[i]
            i += 1
            k += 1
        while j< len(arr2):
            arr[k] = arr2[j]
            k += 1
            j += 1
    return arr

def quickSort_test(arr):
    n = len(arr)
    if n <= 1:
        return arr
    else:
        pivot = arr.pop()
        left = []
        right = []
        for i in arr:
            if i <= pivot:
                left.append(i)
            else:
                right.append(i)
    return quickSort_test(left) + [pivot] + quickSort_test(right)

# O nlogn
def quickSort(arr):
    n = len(arr)
    if n<=1:
        return arr
    else:
        pivot = arr.pop()
    left = []
    right = []
    for i in arr:
        if i < pivot:
            left.append(i)
        else:
            right.append(i)
    return quickSort(left) +  [pivot] + quickSort(right)

    return arr

if __name__ == "__main__":
    arr = [2,5,6,7,3,4]
    print(quickSort_test(arr))