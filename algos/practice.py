# substring search
def search(pat,str):
    n = len(str)
    m = len(pat)

    for i in range(n-m+1):
        if str[i:m+i] == pat:
            print(i)

# hash = c * (26)^index  % prime number
def getHash(c,idx,p):
    return ord(c) * pow(26,idx) % p
# rabin karp
def rabin_karp(pat,str):
    b = 26 # base
    p = 11 # modulus
    hpat = 0  # hash value of pattern
    hfirststr = 0 # hash of first str
    hstr = 0  # hash value of str
    m = len(pat)
    n = len(str)
    # generating hash for pat
    for i in range(m):
        hpat += (b*hpat + ord(pat[i]))%p
        hfirststr += (b*hfirststr + ord(str[i]))%p
    if hpat == hfirststr:
        print(f"match at index 0")

    print(f"hash for {pat} : {hpat}")
    print(f"hash for first {str[:m]} : {hfirststr}")

    # generating has for text in blocks
    '''
    for i in range(m,n-m+1):
        temp = str[i:i+m]
        print(f"generating hash code for {temp}")
        hstr = 0
        for j in range(m):
            hstr += getHash(temp[j],m-j,p)
        if hstr == hpat:
            print(f"match at {i+1}")
    '''
     
     # with sliding window
    for i in range(1,n):   
        firstch = str[i-1]
        lastch = str[i+m-1]
        hfirststr  = hfirststr - getHash(firstch,m-1,p) + getHash(lastch,0,p)
        print(f"{hfirststr} is the hash for {str[i:i+m]}")
        if hfirststr == hpat:
            print(f"at index {i}")

if __name__ == "__main__":
    pat = "ACBD"
    str = "ACDDACBDACBD"
    rabin_karp(pat,str)
    print("HI")