# using iteration
def fib(n):
    num1 = 0
    num2 = 1
    print(num2)
    next_num = num1 + num2
    while next_num < n-num2: # iteration
        num1,num2 = num2,next_num
        next_num = num1 + num2
        print(next_num)

# recursion O(2^n)
def fib_recu(n):
    if n in {0,1}:
        return n # base case
    return fib_recu(n-1) + fib_recu(n-2)
    
    
# memotization : recursion only but we store the results, and no need to calculate them again and again
cache = {0:0,1:1}
def fib_rec_mem(n):
    if n in cache:
        return cache[n]
    cache[n] = fib_rec_mem(n-1)+fib_rec_mem(n-2)
    return cache[n]


# O(n*n)
# select adjacent elements and swap
def bubblesort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(n):
            if arr[i] < arr[j]:
                arr[i], arr[j] = arr[j], arr[i]
    return arr

# O(n*n)
# select min index and swap
def selsort(arr):
    n = len(arr)
    for i in range(n):
        min_index = i
        for j in range(i+1,n):
            if arr[j] < arr[min_index]:
                min_index = j
        arr[i], arr[min_index] = arr[min_index], arr[i]
    return arr

# O(n*n)
# this is inplace. take first element as sorted one, start from 2nd element and compare it with 1st and swap, 
# similaly take thrid and compare with 2nd and 1st, take 4th and compare it with 3rd, 2nd and 1st.
# O(n) --> O(n square)
def inssort(arr):
    n = len(arr)
    for i in range(1,n):
        key = arr[i]
        while i > 0 and arr[i-1] > key:
            arr[i-1],arr[i] = arr[i], arr[i-1]
            i -=1

    return arr

# O(nlogn)
# divide and conquer 
def mergeSort(arr):
    n = len(arr)
    if n > 1:
        print(f"processing : {arr} for length {n}")
        
        half = n//2
        left = arr[:half]
        right = arr[half:]

        #call recursively
        mergeSort(left)
        mergeSort(right)

        i = 0
        j = 0
        k = 0 # merged array index
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                arr[k] = left[i]
                i += 1
            else:
                arr[k] = right[j]
                j += 1
            k += 1
        while i < len(left):
            arr[k] = left[i]
            i += 1
            k += 1
        while j < len(right):
            arr[k] = right[j]
            j += 1
            k += 1
        return arr
    return arr

# this returns pivot index
def quick_sort(arr):
    n = len(arr)
    if n <= 1:
        return arr
    else:
        pivot = arr.pop()
    left = []
    right = []
    n = len(arr)
    for i in range(n):
        if arr[i] < pivot:
            left.append(arr[i])
        else:
            right.append(arr[i])
    return quick_sort(left) + [pivot] + quick_sort(right)

if __name__ == "__main__":
    myarray=[2,5,6,7,3,4]
    print(myarray)
    #myarray = mergeSort(myarray)
    #x= [fib_rec_mem(i) for i in range(10)]
    print(inssort(myarray))
