# Treenode
# preorder, inorder, postorder

class TreeNode:
    def __init__(self,value) -> None:
        self.value = value
        self.left = None
        self.right = None
    
    def insert(self,value):
        if value < self.value:
            if self.left == None:
                self.left = TreeNode(value)
            else:
                self.left.insert(value)
        else:
            if self.right == None:
                self.right = TreeNode(value)
            else:
                self.right.insert(value)
    
    def preorder(self):
        print(self.value)
        if self.left != None:
            self.left.preorder()
        if self.right != None:
            self.right.preorder()
    
    def inorder(self):
        if self.left != None:
            self.left.preorder()
        print(self.value)
        if self.right != None:
            self.right.preorder()


if __name__ == "__main__":
    tree = TreeNode(10)
    tree.insert(2)
    tree.insert(3)
    tree.insert(6)
    tree.insert(8)
    tree.insert(11)

    tree.inorder()