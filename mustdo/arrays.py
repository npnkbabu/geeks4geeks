# continuous sub array of sum value s
def cont_subarra(arr,s):
    n = len(arr)
    for i in range(n):
        j = i+1
        while (j<=n):
            if sum(arr[i:j]) == s:
                return i,j-1
            j += 1
    return None


# largest sum contiguous array, return the sum value
def lar_sum_arr(arr):
    if arr == None:
        return 0
    if len(arr) == 1:
        return arr[0]

    max_sum, current_sum = float("-inf"), 0
    for n in arr:
        current_sum = max(current_sum+n, n) # not to miss max numbers
        max_sum = max(max_sum, current_sum)
    return max_sum
    
def lar_sum_arr_sub_array_brute(arr):
    if arr == None:
        return 0
    if len(arr) == 1:
        return arr[0]
    
    n = len(arr)
    max_sum, current_sum = float("-inf"),0
    for i in range(n):
        current_sum = arr[i]
        for j in range(n):
            current_sum += arr[j]
            if current_sum > max_sum:
                max_sum = current_sum
                print(arr[i:j+1])
    return max_sum

# find missing number from first n (len of arr) integers
# sum of nums - sum of the array
def find_missing(arr):
    if arr == None:
        return None
    n = len(arr)
    if n == 1:
        return arr[0]-1
    n += 1 # as 1 number missing
    return (n*(n+1))//2 - sum(arr)

from datetime import datetime
from functools import wraps

# decorator : any function which return a string is appended by timestamp
def timestamp(fn):
    '''
    This is timestamp fun
    '''
    @wraps(fn)
    def wrapper(s):
        '''
        This is wrapper
        '''
        return datetime.now().strftime("%y-%m-%d %H:%M:%S :") + s
    return wrapper

@timestamp
def myfun(s):
    '''
    This is myfun
    '''
    return s
# check sum value of 2 elements in sorted array with 2 pointer technique
if __name__ == "__main__":
    print(myfun("hello"))
    print(f'{myfun.__name__        =  }')
    print(f'{myfun.__doc__         =  }')
    print(f'{myfun.__annotations__ =  }')
    print(f'{myfun.__dict__        =  }')


