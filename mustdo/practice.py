import re as re
# https://www.geeksforgeeks.org/print-all-prime-numbers-less-than-or-equal-to-n/

# Print all prime numbers less than or equal to N
def checkPrime(n):
    # corner case
    if n <= 1:
        return False
    
    # check from 2 to n-1
    for i in range(2,n): # O(n)
        if n%i == 0:
            return False
    return True

    
def getPrime_naive(n):
    for i in range(2,n+1): # O(n)
        if checkPrime(i) == True:
            print(i, end=" ")
    
# https://www.geeksforgeeks.org/sieve-of-eratosthenes/
# we move each number and mark if its divisible by that number or > square of that number

def SieveOfEratosthenes(n):
    if n <= 2:
        return []
    else:
        is_prime = [True] * (n+1)
        is_prime[0] = False
        is_prime[1] = False
        for i in range(2,n+1):
            # is its not yet processed
            if is_prime[i] == True:
                # from i square to n
                for j in range(i**2,n):
                    # mark all multiples
                    if j %i == 0:
                        is_prime[j] = False
        print(is_prime)
        print([x for (x,y) in enumerate(is_prime) if y == True])

# Count trailing zeroes in factorial of a number
# https://www.geeksforgeeks.org/count-trailing-zeroes-factorial-number/

def fact(n):
    if n == 1:
        return 1
    if n == 2 :
        return 2
    return n * fact(n-1)

def getFactTrailingZeros_naive(n):
    f = fact(n)
    print(f)
    return len(re.findall(r"0*$",str(f))[0])

# we calculate factorial and divide by 10 to get zeros count. 
# consider prime multiples (multiples which are prime numbers) of factorial n. Trailing zero always produced by
# 2 and 5 (both are primes), we taken min no. of multiples of these 2
# number of 0s in fact(n) is no.of 5 multiples of n, as 5 multiples are always <= 2s multiples
def getFactTrailingZeros(n):
    count = 0 
    while n>5:
        n //= 5
        count += 1
    return count

#https://www.geeksforgeeks.org/maximum-sum-such-that-no-two-elements-are-adjacent/
# max sum of 2 adjecent numbers in array
def getMax_naive(arr):
    # make pairs in all elements and compare with max_sum
    max_sum = 0
    n = len(arr)
    i = 0
    #O(n)
    while i< n-1:
        print(arr[i],arr[i+1])
        max_sum = max(max_sum,arr[i]+arr[i+1])
        i += 1
    print(max_sum)


# Maximum sum such that no two elements are adjacent 
# logic : run through array, take 2 vars, 
# maxsum including the current number = max(prev Excl+this number, old inclu)
# maxsum excluding the current number = prev inclu
# so we need to carry or remember (dynamic programming) the maxsum 
# here using iteration with dynamic programming
def maxSumNoAdj(arr):
    n = len(arr)
    incl = arr[0]
    excl = 0

    for i in range(1,n):
        prev_incl = incl
        incl = max(excl+arr[i], prev_incl)
        excl = prev_incl
        prev_incl = incl
        print(arr[i],incl,excl)
    print(max(incl,excl))

# how we do with recursion?
def maxSumNoAdj_rec(idx,arr,n):
    # base case to get out of this function
    if idx >= n:
        return 0
    return max(maxSumNoAdj_rec(idx+2,arr,n)+ arr[idx],  maxSumNoAdj_rec(idx+1,arr,n))

# basci recu
def print1toN(n,count):
    if count > n:
        return
    print(count)
    print1toN(n,count+1)

#recursion with backtracking
def print1toN_backtrack(n,count):
    if count < 1:
        return
    print1toN_backtrack(n,count-1)
    print(count)

from functools import reduce

def GCD(a,b):
    while a % b != 0 :
        a,b = b,(a%b)
    return b

def LCM(a,b):
    return (abs(a*b) / GCD(a,b))

def bts(arr1,arr2):
    lcm = reduce(LCM,arr1)
    gcd = reduce(GCD,arr2)
    temp = lcm
    counter = 0
    multiple_of_lcm = lcm
    while multiple_of_lcm <= gcd:
        if gcd % multiple_of_lcm == 0:
            counter += 1
        multiple_of_lcm += lcm

    print(counter)

if __name__ == "__main__":
    print("Hi")
    arr = [5, 5, 10, 100, 10, 5]
    #maxSumNoAdj(arr)
    #print(maxSumNoAdj_rec(idx=0,arr=arr,n=len(arr)))
    print(GCD(25,240))
