from flask import Flask, redirect,url_for, request

app = Flask(__name__)

@app.route("/")
def helloWorld():
    return "hello world"

if __name__ == "__main__":
    app.run()