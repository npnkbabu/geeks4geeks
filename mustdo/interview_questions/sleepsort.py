# sleep sort
# you have unsorted integer array [3,2,4,1], you create a thread for each integer and sleep for that integer value duration in the thread and return that integer value and put it in a seperate result list. At the end of the program we should see a sorted list in result list

from threading import Thread
import time
from concurrent.futures import ThreadPoolExecutor


def sleep_sort(arr):
    n = len(arr)
    result = []
    threads = []
    for i in range(n):
        thread = Thread(target=thread_fun,args=(arr[i],result))
        threads.append(thread)
        thread.start()
    for th in threads:
        th.join()
    print(result)

def thread_fun(val,res):
    time.sleep(val)
    res.append(val)
    return

def thread_pool_fun(val):
    time.sleep(val)
    return val

def sleep_sort_pool(arr):
    thpool = ThreadPoolExecutor()


if __name__ == "__main__":
    print("hello")
    arr = [3,2,4,1,2]
    sleep_sort(arr)