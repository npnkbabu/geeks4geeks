#https://python-patterns.guide/


from dataclasses import dataclass

class Singleton(object):
    __instance = None
    def __init__(self) -> None:
        raise Exception("object can not be created")
    
    @classmethod
    def instance(cls):
        if cls.__instance == None:
            cls.__instance = cls.__new__(cls)
        return cls.__instance

    
class Singleton_count:
    __instances = []
    __maxCount = 2
    def __init__(self) -> None:
        raise Exception("object creation is not allowed")

    @classmethod
    def instance(cls):
        if len(cls.__instances) < cls.__maxCount:
            newObj = cls.__new__(cls)
            cls.__instances.append(newObj)
            return newObj
        else:
            return cls.__instances[-1]
        
    

if __name__ == "__main__":
    print("hi")
    obj1 = Singleton.instance()
    #obj2 = Singleton.__instance
    #print(obj1 == obj2)
    print(repr(obj1))