#https://superfastpython.com/python-asyncio/

import asyncio
import time
from asyncio import taskgroups

async def work1():
    for i in range(10):
        print (f"{i} from work1")
        await asyncio.sleep(1)
    return "done"

async def work2():
    for i in range(10,20):
        print (f"{i} from work2")
        await asyncio.sleep(1)
    return "done"

async def main():
    futureobject = asyncio.gather(work1(),work2())
    await futureobject
    return futureobject.result()

if __name__ == "__main__":
    res = asyncio.run(main())
    print(res)
    
    
    