# All about threading
from collections.abc import Callable
from threading import Thread
import concurrent.futures
from typing import Any, Iterable, Mapping
import multiprocessing
import time
import os
import threading

counter = 0
threadlock = threading.Lock()
threadCondition = threading.Condition(threadlock)

# Basic thread
class MyThread(Thread):
    global threadlock
    global threadCondition
    data = []
    def __init__(self, group: None = None, target: Callable[..., object] | None = None, name: str | None = None, args: Iterable[Any] = ..., kwargs: Mapping[str, Any] | None = None, *, daemon: bool | None = None) -> None:
        super().__init__(group, target, name, args, kwargs, daemon=daemon)
    
    def run(self) -> None:
        self.conditionalThread(self.data)
        return
        global counter
        while counter < 100:
            time.sleep(1)
            #locked = self.threadlock.acquire(blocking=False)
            #if locked:
            with self.threadlock:
                counter += 1
                print(f"{threading.current_thread().ident} : {counter}")
                #self.threadlock.release()
    
    def conditionalThread(self, data):
        with threadCondition:
            for i in range(10):
                data.append(i)
                time.sleep(1)
            threadCondition.notify()


def fun1(arg1):
    for i in range(arg1):
        print(f"Hello.. i am fun1 : {i}")

def fun2(arg1):
    for i in range(arg1):
        print(f"Hello.. i am fun2 : {i}")

def TimerFun(arg):
    print("Timer started")
    for i in range(arg):
        print(f"{threading.current_thread().ident} --> [i]")

if __name__ == "__main__":
    print("Hi")
    '''
    Br = threading.Barrier(2,fun1)
    ths = Br.wait()
    print(ths)
    '''
    '''
    # Timer threads
    timerth = threading.Timer(10,TimerFun,[10])
    timerth.start()
    time.sleep(11)
    timerth.cancel()
    '''

    '''
    with threadCondition:
        # conditional thread , once job done notity
        th = MyThread()
        th.start()
        threadCondition.wait()
    print(th.data)
    '''
    # synchronize the thread class with multiple threads
    '''
    mythreads = []
    for i in range(3):
        th = MyThread()
        mythreads.append(th)
        th.start()
    for th in mythreads:
        th.join()
    '''
    '''
    th1 = Thread(target=fun1,args=[10])
    th2 = Thread(target=fun2,args=[20])
    start = time.perf_counter()
    th1.start()
    print(f"process id for th1 : {os.getpid()}")
    th2.start()
    print(f"process id for th2: {os.getpid()}")
    th1.join()
    th2.join()
    print(f"both threads completd in {time.perf_counter()-start}")
    '''
    '''

    # create multiple processes
    proc1 = multiprocessing.Process(target=fun1, args=[10])
    proc2 = multiprocessing.Process(target=fun2, args=[20])
    start = time.perf_counter()
    proc1.start()
    print(f"process id : {proc1.pid}")
    proc2.start()
    print(f"process id : {proc2.pid}")
    proc1.join()
    proc2.join()
    print(f"both processes completd in {time.perf_counter()-start}")
    '''



    