# https://superfastpython.com/threadpoolexecutor-in-python/

from concurrent.futures import ThreadPoolExecutor, as_completed
import threading
import os
import requests
import time

def fun1(arg):
    for i in range(arg):
        print(f" {i} : This is from threadid: {threading.current_thread().ident}, thread name {threading.current_thread().name}")
    return [x for x in range(arg)]

def download_img(imgurl):
    return requests.get(imgurl).content

def download_allimg(imglist):
    res = []
    for img in imglist:
        res.append(download_img(img))
    return res

def jobdone(futures):
    print(futures.result())

if __name__ == "__main__":
    print("hi")
    '''
    result = []
    with ThreadPoolExecutor(thread_name_prefix="thread") as pool:
        result = pool.map(fun1,[x for x in range(10)])
    print([x for x in result])
    '''

    # download images from internet using threadpool executor
    img_urls = [
    'https://media.geeksforgeeks.org/wp-content/uploads/20190623210949/download21.jpg',
    'https://media.geeksforgeeks.org/wp-content/uploads/20190623211125/d11.jpg',
    'https://media.geeksforgeeks.org/wp-content/uploads/20190623211655/d31.jpg',
    'https://media.geeksforgeeks.org/wp-content/uploads/20190623212213/d4.jpg',
    'https://media.geeksforgeeks.org/wp-content/uploads/20190623212607/d5.jpg',
    'https://media.geeksforgeeks.org/wp-content/uploads/20190623235904/d6.jpg',
    ]

    # Generally how we download a file or image from internet using http and map 
    '''
    start = time.perf_counter()
    images_threads = []
    for i in img_urls:
        images_threads.append(download_img(i))
    print(f"with single thread it took {(time.perf_counter()-start):.2f} seconds")

    images_pool = []
    start = time.perf_counter()
    with ThreadPoolExecutor() as pool:
        images_pool = pool.map(download_img,img_urls)
    print(f"with threadpoolexecutor it took {(time.perf_counter()-start):.2f} seconds")

    if images_threads == [x for x in images_pool]:
        print("both are same")
    '''

    # how to use submit
    with ThreadPoolExecutor() as pool:
        futureobj = pool.submit(download_allimg,img_urls)
        futureobj.add_done_callback(jobdone)
        



    
