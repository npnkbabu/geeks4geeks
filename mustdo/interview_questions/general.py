# function overloading
# class variable restriction
# object variable restriction

'''
function overloading
'''

from typing import Any, MutableMapping, Union
from functools import singledispatch, singledispatchmethod

def samplefun(param: Union[int,str]):
    print(f"{param}")

@singledispatch
def fun(arg: Any, verbose=False):
    if verbose:
        print(f"arg : {arg}")
    raise NotImplementedError(f"Type {type(arg)} is not implemented")
    

@fun.register
def _(arg:int, verbose=False):
    if verbose:
        print(f"Here\'s is your number : {arg}")
        return
    print(f"number : {arg}")

@fun.register
def _(arg: str, verbose=False):
    if verbose:
        print(f"Here\'s is your Text : {arg}")
        return
    print(f"Text : {arg}")
    
class A:
    @singledispatchmethod
    def fun(self,arg:Any,verbose=False):
        if verbose:
            print(f"arg : {arg}")
        raise NotImplementedError(f"{arg} type is not implemented")
    
    @fun.register(str)
    def _(self,arg):
        print(f"your Text {arg}")
    
    @fun.register(int)
    def _(self,arg):
        print(f"your int {arg}")

# metaclass
import time
def timerdecroator(fun):
    def mytime(start):
        return (time.perf_counter() - start)
    return mytime

@timerdecroator
def myfun(t):
    print("hello")
    return t

def timerdecroator(fun):
    def mytime(start):
        return (time.perf_counter() - start)
    return mytime

@timerdecroator
def myfun(t):
    print("hello")
    return t

#abstract class
from abc import ABC, abstractmethod
class MyAbstractClass(ABC):

    @abstractmethod
    def f(self):
        print(f"This is f abstract method from base")
    
    @abstractmethod
    def g(self):
        print(f"This is g abstract method from base")

class child(MyAbstractClass):
    
    def f(self):
        print(f"This is f abstract method from child")

    def g(self):
        print(f"This is g abstract method from child")


# how to stop creating class variables?
# when you add a class variable after definition, then its coming for object also. How to seal a class not to add any variables?

class metaB(type):  
    base_time = time.perf_counter()

    # preparing namespace for class on which construction happens
    @classmethod
    def __prepare__(metacls, name: str, bases: tuple[type, ...], /, **kwds: Any) -> MutableMapping[str, object]:
        return super().__prepare__(name, bases, **kwds)
    
    # constructing the class
    def __new__(self,name,bases,namespace):
        namespace["__class_load_time__"] = time.perf_counter() - self.base_time
        return super().__new__(self,name,bases,namespace)
    
    def __setattr__(self, name: str, value: Any) -> None:
        raise AttributeError("no more attributes")

class C(metaclass=metaB):
    pass

# how to stop / limit creating object variables
class B:
    def __init__(self) -> None:
        start = time.perf_counter()
        print(f"my type is {type(B)}")
    
    def __setattr__(self, name: str, value: Any) -> None:
        # limit to 2 variables
        if len(self.__dict__.keys()) >= 2:
            raise ValueError("Excess of parameters")
        else:
            self.__dict__[name] = value

def testFun(arg=[]):
    if arg == None:
        arg = []
    arg.append('a')
    print(arg)

import json
import argparse
import sys
import os

if __name__ == "__main__":
    print("hello")

    # taking arguments from sys.argv
    #config_filename = sys.argv[1] # 0 is python filename
    #print(config_filename)

    # using argparse to take arguments from commandline
    parser = argparse.ArgumentParser(description="This is test program")
    parser.add_argument("-c","--configFileName",type=str,help="provide configuration filename")
    args = parser.parse_args()
    
    print(args.configFileName)
    file = os.path.join(os.getcwd(),"mustdo/interview_questions",args.configFileName)
    with open(file,'r') as conf_file:
        conf = json.load(conf_file)
    
    x=2
    y=5
    print(x and y)
    print(x & y)
    print(conf)
    
    lst = [1,2,3]
    testFun()
    testFun()


