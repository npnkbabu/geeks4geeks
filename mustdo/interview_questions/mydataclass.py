from dataclasses import dataclass
import pandas as pd
import numpy as np

@dataclass
class MyData:
    profession:str
    name:str = "myname"

class MyData1:
    def __init__(self,name,profession) -> None:
        self.name = name
        self.profession = profession

if __name__ == "__main__":
    d1 = MyData(name="per1",profession="software engineer")
    d2 = MyData(name="per1",profession="software engineer")
    print(d1==d2)

    d1 = MyData1(name="per1",profession="software engineer")
    d2 = MyData1(name="per1",profession="software engineer")
    print(d1==d2)

    data1 = [1,2,3,4,5]
    data2 = ['a','b','c','d','e']

    df1 = pd.DataFrame(data1)
    print(df1.head)
    df2 = pd.DataFrame(data2,index=[2,3,4,5,6])
    print(df2.head)

    df = pd.concat([df1,df2],axis=1)
    print(df)
    df = df1.join(df2)
    print(df)