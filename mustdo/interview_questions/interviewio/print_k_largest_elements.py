# print k largest elements, means top k largest elements
# https://interviewing.io/mocks/interviewing.io-python-print-k-largest-elements
# a generator
# 1..inf
def myGen(maxnum,step):
    count = 0
    while count < maxnum:
        count += step
        yield count

def capDecorator(fn):
    def capitalize(arg):
        return arg.upper()
    return capitalize

@capDecorator
def sampleFun(s):
    return s

# small question : given integer , replace all 0s in integer with 5, use of strings not allowed
# divide by 10 

# n % 10 to get last digit with counter,
# res = last digit * 10  * counter
def reverse_number(num):
    ans = 0
    while num > 0:
        num,rem = divmod(num,10)
        ans = ans * 10 + rem
    return ans

def replace_0_5(num):
    if num == 0:
        return 5
    ans = 0
    while num > 0:
        rem = num % 10
        if rem == 0 :
            rem = 5
        ans = (ans * 10)  + rem
        num //= 10
    print(reverse_number(ans))

#O(logn)
def replace_0_5_eff(num):
    sign = 1
    if num == 0:
        return 5
    elif num < 0:
        sign = -1

    ans = 0
    exponent = 1
    while num:
        num,r = divmod(num,10)
        if r == 0:
            r = 5
        ans += r*exponent
        exponent *= 10
    return ans*sign

# print k largest elements, means top k largest elements
def kLargest_naive(arr,k):
    arr = sorted(arr,reverse=True)
    print(arr[:k])

# generate first r fibs
def fib_iter(r):
    a = 0
    b = 1
    for i in range(r):
        a,b = b,a+b
    return a

def fib_recu(r):
    if r == 0:
        return 0
    if r == 1 or r == 2:
        return 1
    else:
        return fib_recu(r-1) + fib_recu(r-2)

#O(logn)
def binarySearch(arr,k):
    n = len(arr)
    if n <= 0:
        return False
    if n == 1:
        if arr[0] == k:
            return True
        else:
            return False
    else:
        mid = n//2
        if arr[mid] == k:
            return True
        if arr[mid] > k:
            arr = arr[:mid]
        else:
            arr = arr[mid:]
        return binarySearch(arr,k)


import heapq
if __name__ == "__main__":
    print("hi")
    #num = 4062
    #replace_0_5_eff(num)
    #x = fib_recu(6)
    #print(x)
    '''
    it = myGen(10,2)
    try:
        while True:
            print(next(it))
    except Exception as ex:
        print(ex)
    '''
    #print(sampleFun("hello"))

    arr = [1,3,6,22,39]
    #heapq.heapify(arr)
    #print(heapq.nlargest(2,arr))
    #print(binarySearch(arr,23))

    # k largest elements in array using binarysearch
    


    
    
