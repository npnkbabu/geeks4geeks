# https://interviewing.io/mocks/facebook-python-remove-nth-node-from-end-of-list
# you have linkedlist , you need to remove kth node from back of the linkedlist and return the updated linkedlist
# no cycles in the list
# k might be < lenght of linked list 
# head node can be null

class Node:
    def __init__(self,val) -> None:
        self.val = val
        self.next = None

def printLinkedList(head):
    # print linkedlist
    while head.next:
        print(f"{head.val}", end="-->")
        head = head.next
    print(f"{head.val}")

def deleteKthNode(head:Node, k: int, n: int):
    if k > n or head == None:
        print("Not possible")
        return
    
    
    #  1 2 3 4 5 6 7 8 9 10 --> 3rd node from end means its 8th node n-k+1 = 10-3+1 = 8
    # traverse from end?
    # we need to use 2 pointer technique.
    # left L, right R seperated by k means L + k = R
    # traverse both L and R one by one node, so that at any time the difference between L and R is k
    # at some point you get R = None, then you need to remove L, so always keep a temp for L-1
    temp = head
    L = head
    R = head
    counter = 0
    for i in range(n):
        if i == 0:
            # move L 1 step and move R n+1
            temp = L
            L = L.next
            while counter <= k: # = is becoz we need to move 1 more step
                R = R.next
                counter += 1
        # L and R are initialized
        else:
            if R == None:
                # remove L
                temp.next = L.next
                L = None
                break
            else:
                temp = L
                L = L.next
                R = R.next
    printLinkedList(head)

if __name__ == "__main__":
    print("hi")
    mylist = [1,2,3,4,5,6,7,8,9,10]
    # construct linked list
    root = Node(mylist[0])
    head = root
    for i in mylist[1:]:
        newNode = Node(i)
        root.next = newNode
        root = newNode
    printLinkedList(head)
    
    k = 7 # remove 7th Node from end
    # 1 2 3 4 5 6 7 8 9 10 --> 7th node from end means its 4th node n-k+1 = 10-7+1
    # so if k > n/2 so its better you move from start.
    # if k < n/2 so better to move from end.
    # worst case O(n/2) and space O(1)

    deleteKthNode(head,1,10)



