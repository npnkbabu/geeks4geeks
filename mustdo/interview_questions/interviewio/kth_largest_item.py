# find kth largest item in array
# https://www.youtube.com/watch?v=hGK_5n81drs
# [3,2,1,5,6,4] k=2 ans=5

# 1st solution is sort arry nlog(n) and kth from end is kth largest

# use heap. when we need largest then go with min heap becoz in min heap we eject small items first, similarly when we need smallest item then we got with max heap becoz in max heap we eject largest items first
# now we take array and insert elements one by one until k, if capacity is more than k then we eject item, when we are ejecting the item from min heap, it ejects least element. so we move like this for complete arrya, when we eject last item, heap is left with k largest elements. O(n log(k))
import heapq
def kLargestItems(arr,k):
    n = len(arr)
    pq = []
    heapq.heapify(pq)
    for i in range(n):
        # push the element
        heapq.heappush(pq,arr[i])

        # check if capacity is reached
        if len(pq) > k:
            heapq.heappop(pq)
    print(pq)

# kthe largest item in binary search tree
# for binary search tree , if you traverse inorder (left--> root --> right) it will give in sorted ascending order.
# so traverse untill kth element

class Node:
    counter = 0
    def __init__(self,value=None) -> None:
        self.val = value
        self.left = None
        self.right = None
    def append(self,value):
        if value:
            if self.val > value:
                if self.left == None:
                    self.left = Node(value)
                else:
                    self.left.append(value)
            elif self.val < value:
                if self.right == None:
                    self.right = Node(value)
                else:
                    self.right.append(value)
            else:
                return
        else:
            print("node can not be empty")
    def inorder(self):
        if self.left:
            Node.counter += 1
            self.left.inorder()
        if Node.counter >= 3:
            print(f"result : {self.val}")
            return
        if self.right:
            Node.counter += 1
            self.right.inorder()

def kthSmallesInBST(head,k,counter):
    pass
        

if __name__ == "__main__":
    print("hi")
    arr = [1,4,55,22,6]
    root = Node(arr[0])
    head = root
    n = len(arr)

    #insert nodes
    for i in range(1,n):
        root.append(arr[i])
    
    #print tree inorder
    counter = 1
    head.inorder()
    
   
