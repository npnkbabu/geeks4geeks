import random

def shuffleit(s:str):
    l = list(s)
    random.shuffle(l)
    return ''.join(l)


if __name__ == "__main__":
    print(shuffleit("hello"))